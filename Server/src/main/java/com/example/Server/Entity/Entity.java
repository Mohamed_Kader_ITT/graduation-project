package com.example.Server.Entity;

import javax.persistence.Id;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "demo")
public class Entity {
	@Id
	private int key;
	private String value;
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Entity(int key, String value) {
		super();
		this.key = key;
		this.value = value;
	}
	public Entity() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
