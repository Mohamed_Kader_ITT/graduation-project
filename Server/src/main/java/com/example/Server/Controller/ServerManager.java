package com.example.Server.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.Server.Entity.Entity;
import com.example.Server.Service.Server;

@RestController
public class ServerManager {
	
@Autowired
Server server;

@PutMapping("/PutMethod/{id}/{value}")
public String putdata(@PathVariable(value = "id") int key, @PathVariable(value = "value") String value) {
	return server.datainsert(key,value);
}

@GetMapping("/GetData/{id}")
public String getdata(@PathVariable(value = "id") int key) {
	return server.getdata(key);
}
}
