package com.itt.ics.communication.impl;

import java.net.*;
import java.io.*;

import java.util.*;

import com.itt.ics.cache.CacheManagerImpl;
import com.itt.ics.communication.ICSProtocol;
import com.itt.ics.db.model;
import com.itt.ics.validator.MessageValidatorImpl;

public class ICSProtocolImpl implements ICSProtocol {

	private Socket socket = null;
	private ServerSocket server = null;
	private DataInputStream in = null;
	Map<Integer, String> map = new HashMap<Integer, String>();
	MessageValidatorImpl messageImpl = new MessageValidatorImpl();

	public ICSProtocolImpl(int port) {
		String result = null;
		CacheManagerImpl CacheManagerImpl = new CacheManagerImpl();
		model model = new model();
		System.out.println("System Started");
		String line = "";
		try {
			server = new ServerSocket(port);

			socket = server.accept();
			in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
			PrintStream output = new PrintStream(socket.getOutputStream());
			while (!line.equals("Over")) {
				try {
					line = in.readUTF();
					boolean validateresult = messageImpl.validate(line);
					if (validateresult) {
						String[] dataSplit = line.split(" ");
						model.setKey(Integer.parseInt(dataSplit[2]));
						model.setValue(dataSplit[3]);
						if (dataSplit[1].equalsIgnoreCase("get")) {
							result = CacheManagerImpl.read(model);
						} else if (dataSplit[1].equalsIgnoreCase("put")) {
							CacheManagerImpl.write(model);
							result = model.getValue();
						} else {
							result = "Undefined method";
						}
						output.flush();
						output.println(result);
					}
				} catch (IOException i) {
					System.out.println(i);
				}
			}
			socket.close();
			in.close();
		} catch (IOException i) {
			System.out.println(i);
		}

	}

	public static void main(String args[]) {
		ICSProtocolImpl server = new ICSProtocolImpl(5000);
	}

}
