package com.itt.ics.validator;

public interface MessageValidator {

	public boolean validate(String Message);
}
