package com.itt.ics.validator;

import java.io.UnsupportedEncodingException;

public class MessageValidatorImpl implements MessageValidator {

	@Override
	public boolean validate(String Message) {
		boolean result = false;
		String[] dataSplit = Message.split(" ");
		byte[] KeyByteValue = null;
		byte[] MessageByteValue = null;
		try {
			KeyByteValue = dataSplit[2].getBytes("UTF-8");
			MessageByteValue = dataSplit[3].getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (KeyByteValue.length <= 256 && MessageByteValue.length <= 1000000) {
			result = true;
		}
		return result;
	}

}
