package com.itt.ics.cache;

import java.util.HashMap;
import java.util.Map;

import com.itt.ics.db.DatabaseManagerImpl;
import com.itt.ics.db.model;

public class CacheManagerImpl implements CacheManager {

	DatabaseManagerImpl dao = new DatabaseManagerImpl();
	Map<Integer, String> map = new HashMap<Integer, String>();

	@Override
	public void write(model model) {
		dao.insert(model.getKey(), model.getValue());
		map.put(model.getKey(), model.getValue());

	}

	@Override
	public String read(model model) {
		String result = null;
		model dbData = new model();
		if (map.containsKey(model.getKey())) {
			result = map.get(model.getKey());
		} else {
			dbData = dao.getData(model.getKey());
		}

		if (dbData.getValue() == null) {
			write(model);
			result = model.getValue();
		} else {
			map.put(model.getKey(), model.getValue());
			result = dbData.getValue();
		}
		return result;
	}

}
