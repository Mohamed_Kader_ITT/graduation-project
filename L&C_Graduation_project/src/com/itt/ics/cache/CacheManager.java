package com.itt.ics.cache;

import com.itt.ics.db.model;

public interface CacheManager {

	public void write(model model);

	public String read(model model);
	//public String getElementFromCache(int key);
}
