package com.itt.ics.db;

public interface DatabaseManager {

	void insert(int key, String value);

	model getData(int key);

	void close();

}
