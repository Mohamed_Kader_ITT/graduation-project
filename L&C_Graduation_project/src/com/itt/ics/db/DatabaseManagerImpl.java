package com.itt.ics.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DatabaseManagerImpl implements DatabaseManager {
	Statement stmt = null;
	Connection c = null;

	public void DbConnection() {
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "seunittest");
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
	}

	@Override
	public void insert(int key, String value) {
		DbConnection();
		try {
			stmt = c.createStatement();
			String sql = "INSERT INTO ICSTable (key, value) " + "VALUES (" + key + ", '" + value + "');";
			stmt.executeUpdate(sql);
			System.out.println("Inserted in database and cache");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public model getData(int key) {
		DbConnection();
		model model = new model();
		try {
			stmt = c.createStatement();
			String sql = "SELECT * FROM ICSTable where key = " + key + ";";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				model.setKey(rs.getInt("key"));
				model.setValue(rs.getString("value"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Search Completed");
		return model;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

}
