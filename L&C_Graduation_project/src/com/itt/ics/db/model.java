package com.itt.ics.db;

public class model {
	private int key;
	private String value;

	public model() {
		super();
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public model(int key, String value) {
		super();
		this.key = key;
		this.value = value;
	}

}
