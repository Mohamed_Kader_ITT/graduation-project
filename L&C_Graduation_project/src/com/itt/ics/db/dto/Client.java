package com.itt.ics.db.dto;

import java.net.*;
import java.util.Scanner;
import java.io.*;

public class Client {
	private Socket socket = null;
	private DataInputStream input = null;
	private DataOutputStream out = null;
	private DataInputStream in = null;

	public Client(String address, int port) {
		Scanner choice = new Scanner(System.in);
		System.out.println("Please select method: \n 1.Write Through \n 2.Write Back\n 3.Write Around");
		int number = 0;
		System.out.print("Enter any number: ");
		number = choice.nextInt();;
		switch(number){  
	    case 1: 
		try {
			socket = new Socket(address, port);

			input = new DataInputStream(System.in);

			out = new DataOutputStream(socket.getOutputStream());
		} catch (UnknownHostException u) {
			System.out.println(u);
		} catch (IOException i) {
			System.out.println(i);
		}

		String line = "";

		while (!line.equals("Over")) {
			try {
				in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
				line = input.readLine();
				out.writeUTF(line);
				String response = in.readLine();
				System.out.println("The value is "+ response +"");
			} catch (IOException i) {
				System.out.println(i);
			}
		}

		try {
			input.close();
			out.close();
			socket.close();
		} catch (IOException i) {
			System.out.println(i);
		}
		break;  
	    case 2: System.out.println("Write Back Method");  
	    break;  
	    case 3: System.out.println("Write Around Method");  
	    break;    
	    default:System.out.println("Error: Enter correct choice.");
		}
	}

	public static void main(String args[]) {
		Client client = new Client("127.0.0.1", 5000);
	}
}